# Integration Testing gradle plugin

Run integration tests separated from unit tests

[View presentation](http://slides.com/fkomauli/gradle-plugin-development)

## Build and install plugin

Install the plugin in the local maven repository with:

`gradle build publish`

## Usage

```
/*
Plugins installed locally have to be declared with the old gradle convention
When published, plugins can be applied with:

plugins {
    id 'it.unimi.di.integration-testing' version '1.0-SNAPSHOT'
}
*/

buildscript {
    repositories {
        mavenLocal()
        jcenter()
    }
    dependencies {
        classpath group: 'it.unimi.di', name: 'integration-testing', version: '1.0-SNAPSHOT'
    }
}

apply plugin: 'java'
apply plugin: 'it.unimi.di.integration-testing'

// Test dependencies: those for integration testing can be declared for the 'integration' source set.
dependencies {
   testCompile 'junit:junit:4.12'
   testCompile 'com.github.stefanbirkner:system-rules:1.12.+'
   integrationCompile 'pl.pragmatists:JUnitParams:+'
}
```
