package integration

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test

class IntegrationTestingPlugin implements Plugin<Project> {

    void apply(Project project) {
        declareIntegrationTestsSourceSet(project)
        configureIntegrationDependencies(project)
        project.task('integration', type: IntegrationTesting, dependsOn: 'test')
        checkFailsIfIntegrationFails(project)
        separateTestReports(project)
    }

    /* Declare a new kind of source, in addition to 'main' and 'test'. */
    private void declareIntegrationTestsSourceSet(Project project) {
        project.with {
            sourceSets {
                integration {
                    java {
                        compileClasspath += main.output + test.output
                        runtimeClasspath += main.output + test.output
                        srcDir project.file('src/integration/java')
                    }
                    resources.srcDir project.file('src/integration/resources')
                }
            }
        }
    }

    /*
     * Gradle generates two dependency configurations for the new kind of source.
     * Give the integration tests the same environment of the unit tests.
     * Then the integration test can have additional dependencies in the 'dependencies' section.
     */
    private void configureIntegrationDependencies(Project project) {
        project.with {
            configurations {
                integrationCompile.extendsFrom testCompile
                integrationRuntime.extendsFrom testRuntime
            }
        }
    }

    /* The check task must fail if there are failing integration tests. */
    private void checkFailsIfIntegrationFails(Project project) {
        project.with {
            check.mustRunAfter integration
        }
    }

    /* Separate unit and integration testing reports. */
    private void separateTestReports(Project project) {
        project.with {
            tasks.withType(Test) {
                testLogging {
                    events 'passed', 'standardOut'
                }
                reports.html.destination = file("${reporting.baseDir}/${name}")
            }
        }
    }
}
