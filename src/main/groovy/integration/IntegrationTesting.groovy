package integration

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.testing.Test

class IntegrationTesting extends Test {

    public IntegrationTesting() {
        group 'verification'
        description 'Run integration tests.'
        def integrationSourceSet = project.sourceSets.integration
        testClassesDir = integrationSourceSet.output.classesDir
        classpath = integrationSourceSet.runtimeClasspath
        outputs.upToDateWhen { false }
    }
}
