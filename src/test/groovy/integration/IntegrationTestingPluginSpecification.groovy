package integration

import spock.lang.*
import org.junit.Rule
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import static org.gradle.testkit.runner.TaskOutcome.*

class IntegrationTestingPluginSpecification extends Specification {

    static final String PLUGIN = 'it.unimi.di.integration-testing'
    static final String TASK = 'integration'

    @Rule
    TemporaryFolder project

    void 'up-to-date when no test is present'() {
        given:
        pluginConfiguration()
        GradleRunner gradle = gradleCommand()
        when:
        BuildResult build = gradle.build()
        then:
        build.task(":$TASK").outcome == UP_TO_DATE
    }

    void 'fails when an integration test fails'() {
        given:
        pluginConfiguration()
        failingTestClassUnder 'integration'
        GradleRunner gradle = gradleCommand()
        when:
        BuildResult build = gradle.buildAndFail()
        then:
        build.task(":$TASK").outcome == FAILED
    }

    void 'unit tests are run before integration tests'() {
        given:
        pluginConfiguration()
        passingTestClassUnder 'test'
        passingTestClassUnder 'integration'
        GradleRunner gradle = gradleCommand()
        when:
        BuildResult build = gradle.build()
        then:
        build.task(":$TASK").outcome == SUCCESS
    }

    void 'integration tests are not run after unit tests failure'() {
        given:
        pluginConfiguration()
        failingTestClassUnder 'test'
        passingTestClassUnder 'integration'
        GradleRunner gradle = gradleCommand()
        when:
        BuildResult build = gradle.buildAndFail()
        then:
        build.task(":$TASK") == null
    }

    @Unroll
    void 'integration test asserting that #a == #b fails'() {
        given:
        pluginConfiguration()
        intComparingTestClassUnder 'integration', a, b
        GradleRunner gradle = gradleCommand()
        when:
        BuildResult build = gradle.buildAndFail()
        then:
        build.task(":$TASK").outcome == FAILED
        where:
        a << [1, 2, 3]
        b << [4, 5, 6]
    }

    @Unroll
    void '#sourceSet tests reports are collected in the #sourceSet reports folder'(String sourceSet) {
        given:
        pluginConfiguration()
        passingTestClassUnder sourceSet
        def gradle = gradleCommand()
        when:
        def buildResult = gradle.build()
        then:
        file("build/reports/$sourceSet/index.html").exists()
        where:
        sourceSet << ['test', 'integration']
    }

    void pluginConfiguration() {
        newFile('build.gradle') << """
            plugins {
                id 'java'
                id '$PLUGIN'
            }
            repositories {
                mavenLocal()
            }
            dependencies {
                testCompile 'junit:junit:4.12'
            }
        """
    }

    void passingTestClassUnder(String sourceSet, String name = 'PassingTest') {
        newFile("src/$sourceSet/java/${name}.java") << """
            import org.junit.*;
            public class $name {
                @Test public void pass() { }
            }
        """
    }

    void failingTestClassUnder(String sourceSet, String name = 'PassingTest') {
        newFile("src/$sourceSet/java/${name}.java") << """
            import org.junit.*;
            public class $name {
                @Test public void fail() {
                    Assert.fail();
                }
            }
        """
    }

    void intComparingTestClassUnder(String sourceSet, int a, int b, String name = 'PassingTest') {
        newFile("src/$sourceSet/java/${name}.java") << """
            import org.junit.*;
            public class $name {
                @Test public void fail() {
                    Assert.assertEquals($a, $b);
                }
            }
        """
    }

    GradleRunner gradleCommand() {
        def pluginTestClasspath = System.getProperty('plugin.test.classpath').readLines().collect { new File(it) }
        return GradleRunner.create()
                .withProjectDir(project.root)
                .withPluginClasspath(pluginTestClasspath)
                .withArguments(TASK, '--stacktrace')
    }

    File file(String path) {
        new File(project.root, path)
    }

    File newFile(String path) {
        def file = new File(project.root, path)
        file.parentFile.mkdirs()
        file.createNewFile()
        return file
    }
}
