package integration

import org.junit.Test
import static org.junit.Assert.*
import static org.junit.Assume.*
import org.gradle.testkit.runner.BuildResult
import static org.gradle.testkit.runner.TaskOutcome.*

class IntegrationTestingPluginTest extends PluginTest {

    static final String TASK = 'integration'

    @Test
    void 'up-to-date when no test is present'() {
        BuildResult build = run TASK
        assertEquals(UP_TO_DATE, build.task(":$TASK").outcome);
    }

    @Test
    void 'fails when an integration test fails'() {
        createFailingTestClassUnder 'integration'
        BuildResult build = runFailing TASK
        assertEquals(FAILED, build.task(":$TASK").outcome);
    }

    @Test
    void 'unit tests are run before integration tests'() {
        createPassingTestClassUnder 'test'
        createPassingTestClassUnder 'integration'
        BuildResult build = run TASK
        assertEquals(SUCCESS, build.task(':test').outcome);
    }

    @Test
    void 'integration tests are not run after unit tests failure'() {
        createFailingTestClassUnder 'test'
        createPassingTestClassUnder 'integration'
        BuildResult build = runFailing TASK
        assumeTrue(FAILED == build.task(':test').outcome);
        assertNull(build.task(":$TASK"))
    }

    @Test
    void 'unit tests reports are collected in a separate folder'() {
        createPassingTestClassUnder 'test'
        BuildResult build = run 'test'
        assumeTrue(SUCCESS == build.task(":test").outcome)
        assertTrue(file("build/reports/test/index.html").exists())
    }

    @Test
    void 'integration tests reports are collected in a separate folder'() {
        createPassingTestClassUnder 'integration'
        BuildResult build = run TASK
        assumeTrue(SUCCESS == build.task(":$TASK").outcome)
        assertTrue(file("build/reports/integration/index.html").exists())
    }
}
